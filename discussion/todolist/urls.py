from django.urls import path

from . import views


urlpatterns = [
    path('', views.index, name='index'),
    #/todolist/<todoitem_id>
    #/todolist/1
    # The <int:todoitem_id> allows for creating a dynamic link where the todoitem_id is provided
    path('<int:todoitem_id>/', views.todoitem, name='viewtodoitem'),
]
