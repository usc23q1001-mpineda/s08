from django.shortcuts import render
from django.http import HttpResponse
from .models import ToDoItem
from django.template import loader

def index(request):
    todoitem_list = ToDoItem.objects.all()
    context = {'todoitem_list': todoitem_list}
    return render(request, "todolist/index.html", context)


    # return HttpResponse(template.render(context, request))
    #   template = loader.get_template("todolist/index.html")

    #    output = ', '.join([todoitem.task_name for todoitem in todoitem_list])   
    #    return HttpResponse(output) 
    
    #  return HttpResponse("Hello from the views.py file")


def todoitem(request, todoitem_id):
    response = "You are viewing the details of %s"
    return HttpResponse(response % todoitem_id)
